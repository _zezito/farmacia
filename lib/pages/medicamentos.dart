import 'package:flutter/cupertino.dart';

class Medicamentos {
  final String id;
  final String nome;
  final String preco;
  final String preco2;
  final String modoUsar;
  final String quantidade;
  
  const Medicamentos({
    this.id,
    @required this.nome,
    @required this.preco,
    @required this.preco2,
    @required this.modoUsar,
    @required this.quantidade,
  });
}
