import 'dart:math';
import 'package:farmacia/data/dummy_medicamentos.dart';
import 'package:farmacia/pages/medicamentos.dart';
import 'package:flutter/material.dart';

class Users with ChangeNotifier {
  final Map<String, Medicamentos> _items = {...DUMMY_MEDICAMENTOS};

  List<Medicamentos> get all {
    return [..._items.values];
  }

  int get count {
    return _items.length;
  }

  Medicamentos byIndex(int i) {
    return _items.values.elementAt(i);
  }

  put(Medicamentos medicamentos) {
    if (medicamentos == null) {
      return null;
    }
     else if (medicamentos.id != null &&
        medicamentos.id.trim().isNotEmpty &&
        _items.containsKey(medicamentos.id)) {
      _items.update(
        medicamentos.id,
        (_) => Medicamentos(
          id: medicamentos.id,
          nome: medicamentos.nome,
          preco: medicamentos.preco,
          preco2: medicamentos.preco2,
          modoUsar: medicamentos.modoUsar,
          quantidade: medicamentos.quantidade,
        ),
      );
    } else {

      //adicionar medicamento
      final id = Random().nextDouble().toString();
      _items.putIfAbsent(
        id,
        () => Medicamentos(
          id: id,
          nome: medicamentos.nome,
          preco: medicamentos.preco,
          preco2: medicamentos.preco2,
          modoUsar: medicamentos.modoUsar,
          quantidade: medicamentos.quantidade,
        ),
      );
    }
    notifyListeners();
  }

  void remove(Medicamentos medicamentos) {
    if (medicamentos != null && medicamentos.id != null) {
      _items.remove(medicamentos.id);
      notifyListeners();
    }
  }
}
