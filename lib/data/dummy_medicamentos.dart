import 'package:farmacia/pages/medicamentos.dart';

const DUMMY_MEDICAMENTOS = {
  '1': const Medicamentos(
    id: '1',
    nome: 'dipirona',
    preco: '2',
    preco2: '1.5',
    modoUsar: 'teste',
    quantidade: '20',
  ),
  '2': const Medicamentos(
    id: '2',
    nome: 'diasepam',
    preco: '2',
    preco2: '1.5',
    modoUsar: 'teste',
    quantidade: '20',
  ),
  '3': const Medicamentos(
    id: '3',
    nome: 'neosoro',
    preco: '2',
    preco2: '1.5',
    modoUsar: 'teste',
    quantidade: '20',
  ),
  '4': const Medicamentos(
    id: '4',
    nome: 'neosoro',
    preco: '2',
    preco2: '1.5',
    modoUsar: 'teste',
    quantidade: '20',
  ),
};
