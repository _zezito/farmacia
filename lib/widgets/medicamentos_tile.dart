import 'package:farmacia/pages/medicamentos.dart';
import 'package:farmacia/provider/users.dart';
import 'package:farmacia/routes/app_routes.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../views/user_form.dart';

class MedicamentosTile extends StatelessWidget {
  final Medicamentos medicamentos;

  const MedicamentosTile(this.medicamentos);

  @override
  Widget build(BuildContext context) {
    return ListTile(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => UserForm(
                    editingForm: false,
                    conteudo: medicamentos,
                  )),
        );
      },
      title: Text(medicamentos.nome),
      subtitle: medicamentos.quantidade != ''
          ? Text('Quantidade: ${medicamentos.quantidade}')
          : Text(
              'Indisponível',
              style: TextStyle(color: Colors.red),
            ),
      trailing: Container(
        width: 100,
        child: Row(
          children: <Widget>[
            IconButton(
              icon: Icon(Icons.edit),
              color: Colors.orange,
              onPressed: () {
                Navigator.of(context).pushNamed(
                  AppRoutes.USER_FORM,
                  arguments: medicamentos,
                );
              },
            ),
            IconButton(
              icon: Icon(Icons.delete),
              color: Colors.red,
              onPressed: () {
                showDialog(
                  context: context,
                  builder: (ctx) => AlertDialog(
                    title: Text('Excluir medicamento'),
                    content: Text('Tem certeza?'),
                    actions: <Widget>[
                      FlatButton(
                        child: Text('Sim'),
                        onPressed: () {
                          Provider.of<Users>(context, listen: false)
                              .remove(medicamentos);
                          Navigator.of(context).pop();
                        },
                      ),
                      FlatButton(
                        child: Text('Não'),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      )
                    ],
                  ),
                );
              },
            ),
          ],
        ),
      ),
    );
  }
}
