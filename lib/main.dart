import 'package:farmacia/provider/users.dart';
import 'package:farmacia/routes/app_routes.dart';
import 'package:farmacia/views/medicamentos_list.dart';
import 'package:farmacia/views/user_form.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (ctx) => Users(),
        ),
      ],
      child: MaterialApp(
          title: 'Farmacia Demo',
          theme: ThemeData(
            primarySwatch: Colors.blue,
            visualDensity: VisualDensity.adaptivePlatformDensity,
          ),
          routes: {
            AppRoutes.HOME: (_) => MedicamentosList(),
            AppRoutes.USER_FORM: (_) => UserForm()
          }),
    );
  }
}
