import 'package:farmacia/pages/medicamentos.dart';
import 'package:farmacia/provider/users.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';

var _conteudo;
bool _editingForm = true;

class UserForm extends StatefulWidget {
  UserForm({ bool editingForm: true, conteudo}) {
    _editingForm = editingForm;
    _conteudo = conteudo;
  }
  @override
  _UserFormState createState() => _UserFormState();
}

class _UserFormState extends State<UserForm> {
  final _form = GlobalKey<FormState>();

  final Map<String, String> _formData = {};

  void _loadFormData(Medicamentos medicamentos) {
    if (medicamentos != null) {
      _formData['id'] = medicamentos.id;
      _formData['nome'] = medicamentos.nome;
      _formData['preco'] = medicamentos.preco;
      _formData['preco2'] = medicamentos.preco2;
      _formData['quantidade'] = medicamentos.quantidade;
      _formData['modoUsar'] = medicamentos.modoUsar;
    }
  }

  @override
  Widget build(BuildContext context) {
    final Medicamentos medicamentos = ModalRoute.of(context).settings.arguments;

    _loadFormData(medicamentos);

    return Scaffold(
      appBar: AppBar(
        title: Text('Ficha do Medicamento'),
        actions: <Widget>[
          _editingForm
              ? IconButton(
                  icon: Icon(Icons.save),
                  onPressed: () {
                    final isValid = _form.currentState.validate();

                    if (isValid) {
                      _form.currentState.save();

                      Provider.of<Users>(context, listen: false).put(
                        Medicamentos(
                          id: _formData['id'],
                          nome: _formData['nome'],
                          preco: _formData['preco'],
                          preco2: _formData['preco2'],
                          modoUsar: _formData['modoUsar'],
                          quantidade: _formData['quantidade'],
                        ),
                      );

                      Navigator.of(context).pop();
                    }
                  },
                )
              : Container()
        ],
      ),
      body: ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.all(15),
            child: Form(
              key: _form,
              child: Column(
                children: <Widget>[
                  TextFormField(
                    enabled: _editingForm,
                    initialValue:
                        _editingForm ? _formData['nome'] : _conteudo.nome,
                    decoration: InputDecoration(labelText: 'Nome'),
                    validator: (value) {
                      if (value == null || value.trim().isEmpty) {
                        return 'Informe o nome do medicamento!';
                      }

                      return null;
                    },
                    onSaved: (value) => _formData['nome'] = value,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp(r"[\d.]"))
                    ],
                    maxLength: 5,
                    enabled: _editingForm,
                    initialValue:
                        _editingForm ? _formData['preco'] : _conteudo.preco,
                    decoration: InputDecoration(labelText: 'Preço'),
                    onSaved: (value) => _formData['preco'] = value,
                  ),
                  TextFormField(
                    keyboardType: TextInputType.number,
                    maxLength: 5,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp(r"[\d.]"))
                    ],
                    enabled: _editingForm,
                    initialValue:
                        _editingForm ? _formData['preco2'] : _conteudo.preco2,
                    decoration:
                        InputDecoration(labelText: 'Preço com desconto'),
                    onSaved: (value) => _formData['preco2'] = value,
                  ),
                  TextFormField(
                    maxLength: 5,
                    inputFormatters: [
                      WhitelistingTextInputFormatter(RegExp(r"[\d]"))
                    ],
                    keyboardType: TextInputType.number,
                    enabled: _editingForm,
                    initialValue: _editingForm
                        ? _formData['quantidade']
                        : _conteudo.quantidade,
                    decoration: InputDecoration(labelText: 'Quantidade'),
                    onSaved: (value) => _formData['quantidade'] = value,
                  ),
                  TextFormField(
                    enabled: _editingForm,
                    initialValue: _editingForm
                        ? _formData['modoUsar']
                        : _conteudo.modoUsar,
                    decoration: InputDecoration(labelText: 'Modo de Usar'),
                    onSaved: (value) => _formData['modoUsar'] = value,
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
