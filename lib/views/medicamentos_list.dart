import 'package:farmacia/provider/users.dart';
import 'package:farmacia/routes/app_routes.dart';
import 'package:farmacia/widgets/medicamentos_tile.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class MedicamentosList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final Users users = Provider.of(context);

    return Scaffold(
      appBar: AppBar(
        title: Text('Lista de Medicamentos'),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.add),
            onPressed: () {
              Navigator.of(context).pushNamed(
                AppRoutes.USER_FORM,
              );
            },
          ),
        ],
      ),
      body: ListView.builder(
        itemCount: users.count,
        itemBuilder: (ctx, i) => MedicamentosTile(users.byIndex(i)),
      ),
    );
  }
}
